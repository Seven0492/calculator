mod doc;
pub mod module;

#[derive(Debug)]
pub struct NumSet {
    num1: String,
    symbol: String,
    num2: String,
}

pub fn run(args: Vec<String>, i: usize, passed_over_initial_block: usize, do_not_print: u8) {
    if cfg!(debug_assertions) {
        println!("Initial args variable: {:?}", &args);
    }

    let mut search_for_symbol = 0;

    let mut set = NumSet {
        num1: String::new(),
        symbol: String::new(),
        num2: String::new(),
    };

    for (index, n) in args.iter().enumerate() {
        // Debugging
        if cfg!(debug_assertions) {
            println!("Variable n: {}", &n);
        }
        // EOD

        // Debugging
        if cfg!(debug_assertions) {
            println!("args.len(): {}", args.len());
            println!("Index: {}", &index);
        }
        // EOD

        if passed_over_initial_block != i {
            // Debugging
            if cfg!(debug_assertions) {
                println!("Restarting loop");
            }
            // EOD

            let mut new_args = args.clone();

            new_args.remove(0);

            run(new_args, i, passed_over_initial_block + 1, 0);

            return;
        }

        if n.parse::<f64>().is_ok() {
            if search_for_symbol > 0 {
                if cfg!(debug_assertions) {
                    eprintln!("Searching for symbol but finding a number");
                }

                doc::wrong_formating(1);

                // Debugging
                if cfg!(debug_assertions) {
                    println!("search_for_symbol: '{}'", &search_for_symbol);
                }
                // EOD

                doc::help();

                return;
            } else {
                // Debugging
                if cfg!(debug_assertions) {
                    println!("Old search_for_symbol: '{}'", &search_for_symbol);
                }
                // EOD

                search_for_symbol = 1;

                // Debugging
                if cfg!(debug_assertions) {
                    println!("New search_for_symbol: '{}'", &search_for_symbol);
                }
                // EOD

                if set.num1 == "" {
                    // Debugging
                    if cfg!(debug_assertions) {
                        println!("Cloning 'n' into set.num1 which is: {}", &set.num1);
                    }
                    // EOD

                    set.num1 = n.clone();

                    // Debugging
                    if cfg!(debug_assertions) {
                        println!("New set.num1: {}", &set.num1);
                        println!("{:?}", &set);
                    }
                    // EOD
                } else if set.num2 == "" {
                    // Debugging
                    if cfg!(debug_assertions) {
                        println!("Cloning 'n' into set.num2 which is: {}", &set.num2);
                    }
                    // EOD

                    set.num2 = n.clone();

                    // Debugging
                    if cfg!(debug_assertions) {
                        println!("New set.num2: {}", &set.num2);
                        println!("{:?}", &set);
                    }
                    // EOD
                } else {
                    // Debugging
                    if cfg!(debug_assertions) {
                        println!("Cloning 'n' into set.num1 which is: {}", &set.num1);
                    }
                    // EOD

                    set.num1 = n.clone();

                    // Debugging
                    if cfg!(debug_assertions) {
                        println!("New set.num1: {}", &set.num1);
                        println!("Emptying set.num2: {}", &set.num2);
                        println!("Emptying set.symbol: {}", &set.symbol);
                    }
                    // EOD

                    set.num2 = String::new();
                    set.symbol = String::new();

                    // Debugging
                    if cfg!(debug_assertions) {
                        println!("New set.num2: {}", &set.num2);
                        println!("New set.symbol: {}", &set.num2);
                        println!("{:?}", &set);
                    }
                    // EOD
                }
            }

            if (index + 1) % 3 == 0 && index != 0 {
                prep(&args, &set, &i, &passed_over_initial_block, do_not_print);

                return;
            }
        } else {
            if set.num1 != "" && set.num2 != "" && set.symbol != "" && search_for_symbol == 1 {
                prep(&args, &set, &i, &passed_over_initial_block, do_not_print);

                return;
            } else if set.num2 == "" && set.symbol == "" && set.num1 != "" && search_for_symbol == 1
            {
                // Debugging
                if cfg!(debug_assertions) {
                    println!("Old search_for_symbol: {}", &search_for_symbol);
                }
                // EOD

                search_for_symbol = 0;

                // Debugging
                if cfg!(debug_assertions) {
                    println!("New search_for_symbol: {}", &search_for_symbol);
                    println!("Cloning 'n' into empty set.symbol");
                }
                // EOD

                set.symbol = n.clone();

                // Debugging
                if cfg!(debug_assertions) {
                    println!("New set.symbol: {}", &set.symbol);
                    println!("{:?}", &set);
                }
                // EOD
            } else {
                doc::wrong_formating(1);

                // Debugging
                println!("search_for_symbol: '{}'\n", &search_for_symbol);
                // EOD

                doc::help();

                return;
            }

            // If index is divisible by 3 (ex: 3, 6, 9, 12...)
            if (index + 1) % 3 == 0 {
                prep(&args, &set, &i, &passed_over_initial_block, do_not_print);

                return;
            }
        }
    }
}

fn prep(
    args: &Vec<String>,
    set: &NumSet,
    i: &usize,
    passed_over_initial_block: &usize,
    mut do_not_print: u8,
) {
    // Debugging
    if cfg!(debug_assertions) {
        println!("Old do_not_print: {}", do_not_print);
    }
    // EOD

    if do_not_print == 1 {
        if args.len() == 3 {
            do_not_print -= 1;
        }
    } else if do_not_print == 0 {
        if args.len() != 3 {
            do_not_print += 1;
        }
    } else {
        doc::wrong_formating(1);
        doc::help();
    }

    if cfg!(debug_assertions) {
        println!("New do_not_print: {}", do_not_print);
        println!(
            "Soon to be passed Vec<String>: {:?}",
            vec![set.num1.clone(), set.symbol.clone(), set.num2.clone()]
        );
        println!("Soon to be passed do_not_print: {}", do_not_print);
        println!("Passing into src/iterate/module/mod.rs::operations()\n");
    }

    let result = module::operations(
        vec![set.num1.clone(), set.symbol.clone(), set.num2.clone()],
        0,
        do_not_print.clone(),
    )
    .1;

    // Debugging
    if cfg!(debug_assertions) {
        println!("Variable result: {}", result);
    }
    // EOD

    let mut new_args = args.clone();

    for _ in 0..2 {
        new_args.remove(0);
    }

    new_args[0] = result.to_string();

    // Debug
    if cfg!(debug_assertions) {
        println!("new_args.len(): {}", new_args.len());
    }
    // EOD

    if new_args.len() == 3 {
        run(new_args, i.clone(), passed_over_initial_block.clone(), 0);
    } else {
        run(new_args, i.clone(), passed_over_initial_block.clone(), 1);
    }
}
